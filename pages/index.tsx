import React, { useEffect, useState } from "react";

import { Inter } from "next/font/google";
import axios from "axios";

export default function Home() {
  const [hasDemoFeature, setHasDemoFeature] = useState(false);

  useEffect(() => {
    const fetchFeatureFlags = async () => {
      const projectId = "44177338";
      const apiUrl = `https://gitlab.com/api/v4/projects/${projectId}/feature_flags`;

      try {
        const response = await axios.get(apiUrl, {
          headers: {
            "Private-Token": process.env.NEXT_PUBLIC_GITLAB_ACCESS_TOKEN,
          },
        });

        const fetchedFeatureFlags = response.data;
        if (fetchedFeatureFlags) {
          const demoFeature = fetchedFeatureFlags.find(
            (flag: { name: string }) => flag.name === "demo_feature"
          );
          if (demoFeature) {
            setHasDemoFeature(demoFeature.active);
          }
        }
      } catch (error) {
        console.error("Failed to fetch feature flags:", error);
      }
    };

    fetchFeatureFlags();
  }, []);

  return (
    <div className="container">
      <h1>
        Welcome to a{" "}
        <a
          className="gitlab"
          href="https://about.gitlab.com/"
          rel="noreferrer noopener"
          target="_blank"
        >
          GitLab
        </a>{" "}
        CI/CD demo 🦊
      </h1>
      <br />
      <h2>Version: </h2>
      <br />
      {hasDemoFeature && <h2>Feature flag active 🏁</h2>}
    </div>
  );
}
