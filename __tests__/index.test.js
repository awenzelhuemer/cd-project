import { render, screen } from "@testing-library/react";
import Home from "@/pages/index";

describe("Home", () => {
  it("should render the correct welcome text", () => {
    render(<Home />);
    const welcomeText = screen.getByText("Welcome to a CI/CD demo 🦊");
    expect(welcomeText).toBeInTheDocument();
  });

  it("should include a gitlab link", () => {
    render(<Home />);
    const gitlabLink = screen.getByRole("link", { name: /gitlab/i });
    expect(gitlabLink).toBeInTheDocument();
    expect(gitlabLink).toHaveAttribute("href", "https://about.gitlab.com/");
  });
});
