# Continuous delivery project

This is a project for the course Continuous Delivery in agile Software Development in the summer semester 2023.

## Pipeline

### Run pipeline locally

There is a local Docker project runner set up for running the pipeline. To run it, the local Docker container has to be running. A docker-compose file for a local runner can be found under gitlab-runner folder.

1. Starting the container:
```shell
cd gitlab-runner
docker-compose up -d
```

2. Create a new authentication token:

- https://gitlab.com/awenzelhuemer/cd-project/-/runners/new
- For platform choose Linux and select the option "Run untagged jobs".

3.  Registering the runner:
```shell
docker exec -it gitlab-runner gitlab-runner register  --url https://gitlab.com  --token < auth-token > --executor docker --docker-image alpine:latest --non-interactive
```

 For more information see the documentation for more information.

- Docker setup: https://docs.gitlab.com/runner/install/docker.html#option-1-use-local-system-volume-mounts-to-start-the-runner-container
- Registering a runner: https://docs.gitlab.com/runner/register/#docker

### Pipeline steps


- Linting
- Testing
  - Unit tests
  - Sonar cloud scan on MR and commit on main: https://sonarcloud.io/project/branches_list?id=cicd23_cicdproject
- Review
  - Deploys review app in temp environment on MR
- Modify code
  - Adds version from package.json to web application frontend on the main branch when a new release tag is created
- Publish
  - Builds and publishes on the main branch when a new release tag is created
- Deploy
  - Deploys to GitLab Pages on the main branch when a new release tag is created
  - Deploys to Azure App Services on the main branch when a new release tag is created
  - Deploys package_project when merged onto main branch
    - for installing in the web application: https://docs.gitlab.com/ee/user/packages/npm_registry/#authenticate-to-the-package-registry 

## Deployments

- GitLab Pages: https://awenzelhuemer.gitlab.io/cd-project/
- Azure App Services: https://mango-tree-0a7f3f103.3.azurestaticapps.net/

## Feature flag (locally running application)

The **demo_feature** can be enabled here: https://gitlab.com/awenzelhuemer/cd-project/-/feature_flags. It is available after reloading the application locally.

## Local setup

### Install from package registry

Authenticate to the package registry: https://docs.gitlab.com/ee/user/packages/npm_registry/#authenticate-to-the-package-registry

### Get feature flags

For getting the feature flag from git GitLab API, you need to set `NEXT_PUBLIC_GITLAB_ACCESS_TOKEN=< access-token >` in a **.env.local** file.
